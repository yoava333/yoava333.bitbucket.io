(function () {
  var app = angular.module("CogApp", []);

  app.controller("CogCtrl", function ($scope, $timeout) {
    var circles = 3;


    $scope.timeout = 3;
    $scope.test_len = 5;
    $scope.data = [0, 0, 0];
    $scope.value = [0, 0, 0];
    $scope.real = [];
    $scope.ans = ["", "", ""];
    $scope.input_answer = false;
    // $scope.input_answer = true;
    $scope.idx = -1;

    $scope.startTest = function() {
      $scope.ans = ["", "", ""];
      $scope.real = [];
      $scope.value = [0, 0, 0];
      $scope.input_answer = false;
      $scope.data = []; 
      $scope.idx = 0;

      for (var i = 0; i < circles; i++) {
	$scope.data[i] = [];
	var sum = 0;
	for (var j = 0; j < $scope.test_len; j++) {
	  var rand = Math.ceil(Math.random() * 10);
	  $scope.data[i].push(rand);
	  sum += rand;
	}
	console.log(sum);
	$scope.real.push(sum);
      }
      
      $timeout($scope.nextNumber, $scope.timeout*1000);
    }

    $scope.nextNumber = function() {
      if ($scope.idx < $scope.test_len) {
	$scope.idx += 1;
	if ($scope.idx == $scope.test_len) {
		$scope.input_answer = true;
	}

	$timeout($scope.nextNumber, $scope.timeout*1000);
	return;
      }

    }

    $scope.checkAnswer = function() {
	for (var i = 0; i < circles; i++) {
		$scope.ans[i] = $scope.value[i] == $scope.real[i] ? "green" : "red";
		$scope.data[i].push($scope.real[i]);
	}
    }
  });
})();
